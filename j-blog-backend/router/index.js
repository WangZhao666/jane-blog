// 引入 Koa Router 库
const Router = require('@koa/router')
// 引入 controller 模块
const homeCtrl = require('../controller/home')
// 引入 auth 模块
const authCtrl = require('../controller/auth')
// 引入 分类 模块
const categoryCtrl = require('../controller/categories')
// 引入 上传 模块
const uploadCtrl = require('../controller/upload')
// 引入 文章 模块
const articleCtrl = require('../controller/article')
// 引入 用户 模块
const profileCtrl = require('../controller/profile')
// 引入 博客 模块
const blogCtrl = require('../controller/blog')

// 创建路由器实例
const router = new Router()

// 配置路由
// - 测试用的 API
router.get('/api/test', homeCtrl.test)
router.get('/api/user/test', homeCtrl.test2)

// 注册
router.post('/api/register', authCtrl.register)
// 图片验证码
router.get('/api/captcha', authCtrl.captcha)
// 登录
router.post('/api/login', authCtrl.login)

// 获取所有文章分类
router.get('/api/categories', categoryCtrl.list)

// 上传图片
router.post('/api/user/image/upload', uploadCtrl.uploadImage)
// 读取图片
// router.get('/image/:name', uploadCtrl.image)

// 新建文章
router.post('/api/user/articles', articleCtrl.create)
// 查询自己的文章列表
router.get('/api/user/articles', articleCtrl.list)
// 获取自己的文章详情
router.get('/api/user/articles/:id', articleCtrl.detail)
// 删除自己的文章
router.delete('/api/user/articles/:id', articleCtrl.remove)
// 编辑自己的文章
router.put('/api/user/articles/:id', articleCtrl.update)

// 获取用户信息
router.get('/api/user/profile', profileCtrl.getProfile)
// 修改用户信息
router.put('/api/user/profile/baseinfo', profileCtrl.updateProfileBaseInfo)
// 修改密码
router.put('/api/user/profile/password', profileCtrl.updateProfilePassword)
// 修改头像
router.put('/api/user/profile/avatar', profileCtrl.updateProfileAvatar)

// 根据分类获取博客
router.get('/api/articles', blogCtrl.listArticlesByCategory)
// 获取文章详情
router.get('/api/articles/:id', blogCtrl.getArticleDetail)

// 导出
module.exports = router