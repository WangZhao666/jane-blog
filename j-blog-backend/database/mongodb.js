// 引入MongoDB驱动包
const { MongoClient } = require('mongodb')
// 引入配置文件
const { mongodb: config } = require('../config/config.default')

// 创建MongoDB客户端实例,相关配置信息从配置文件中获取
const client = new MongoClient(config.uri, config.options)

// 连接 MongoDB 服务器
client.connect()

// 导出客户端实例
module.exports = client