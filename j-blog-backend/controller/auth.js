const { doRegister, generateCaptcha, doLogin } = require("../service/auth")

module.exports = {

    // 注册
    async register(ctx) {
        // 校验参数
        ctx.verifyParams({
            username: 'string',
            nickname: 'string',
            password: 'password'
        })

        // 调用 service 进行用户注册
        await doRegister(ctx, ctx.request.body)

        // 返回成功数据
        ctx.body = {
            code: 0,
            message: '注册成功',
            data: true
        }
    },
    // 生成图片验证码
    async captcha(ctx) {
        // 生成验证码
        const result = await generateCaptcha(ctx)

        // 返回成功数据
        ctx.body = {
            code: 0,
            message: '获取验证码成功',
            data: result
        }
    },
    // 登录
    async login(ctx) {
        // 校验参数
        ctx.verifyParams({
            username: 'string',
            password: 'password',
            captchaKey: 'string',
            captchaCode: 'string'
        })

        // 登录并获取 JWT Token
        const token = await doLogin(ctx, ctx.request.body)

        // 返回成功数据
        ctx.body = {
            code: 0,
            message: '登录成功',
            data: token
        }
    },

}