module.exports = {

    // 测试用的处理函数
    async test(ctx) {
        // 方式一: 手动抛出异常
        // throw new Error('出错啦')
        // 方式二：Koa 封装的抛出错误工具函数
        // return ctx.throw({ code: 40011, message: '一个错误2' })

        // 请求参数的校验：是否存字符串类型的 name 参数；校验失败会抛出 Error 对象
        ctx.verifyParams({
            name: 'string'
        })

        // 打印实例
        // console.log(ctx.mongoClient);
        ctx.body = {
            msg: 'hello,world'
        }
    },
    // 测试token
    async test2(ctx) {
        console.log('token携带信息', ctx.state);
        ctx.body = {
            msg: 'ok'
        }
    }

}