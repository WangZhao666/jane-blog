const { getAllCategories } = require("../service/categories")

module.exports = {
    // 获取所有文章分类
    async list(ctx) {
        // 获取所有分类
        const categories = await getAllCategories(ctx)

        // 返回成功数据
        ctx.body = {
            code: 0,
            message: '获取分类成功',
            data: categories
        }
    }
}