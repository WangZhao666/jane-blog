// 导入文件读取模块
// const fs = require('fs')
// 导入路径处理模块
// const { resolve } = require('path')

// 获取静态资源图片的方法
// function getImageFile(filePath) {
//     return new Promise((resolve, reject) => {
//         fs.readFile(filePath, (err, data) => {
//             if (err) {
//                 reject(err)
//             } else {
//                 // 这里保持 Buffer 格式数据，因为图片是二进制数据，不要转成字符串
//                 resolve(data)
//             }
//         })
//     })
// }

module.exports = {
    // 上传图片
    async uploadImage(ctx) {
        // 获取上传的文件信息
        const file = ctx.request.files.file

        const location = '/uploads/' + file.newFilename

        // 返回成功数据
        ctx.body = {
            code: 0,
            message: '上传成功',
            data: {
                location
            }
        }
    },
    // 读取图片
    // async image(ctx) {
    //     // 设置响应头,否则在浏览器只会下载图片,不能看到图片
    //     ctx.set('Content-Type', 'image/jpeg')
    //     ctx.body = await getImageFile(resolve(__dirname,'../static/uploads',ctx.params.name))
    // }

}