// 本文件, 配置导航守卫, 控制访问权限
import router from './router/index'
import store from './store'

// 配置全局前置守卫
// 判断有没有token
// 1. 有token
//    (1) 如果是登录页, 直接进入后台首页
//    (2) 其他页面, 获取个人信息, 放行
// 2. 没有token
//    (1) 如果是后台的页面, 需要权限访问的页面 /my, 拦截到登录
//    (2) 如果是公开的, 直接放行
router.beforeEach(async (to, from, next) => {

  const token = store.state.token

  // 判断有无token
  if (token) {
    // 是否是登录页
    if (to.path === '/sign/login') {
      next('/my/articles')
    }
    // 其他页面, 获取个人信息(检测是否已经有个人信息)
    else {
      const userInfo = store.state.userInfo
      // 判断是否存在用户信息
      if (userInfo.username) {
        next()
      }
      // 不存在个人信息, 则获取
      else {
        try {
          await store.dispatch('getUserInfo')
          // 获取个人信息完毕, 这里重新进入一下路由
          // 为了保证进入界面, 展示的所有组件, 是根据最新的数据存储去展示的, 
          // 如果有动态路由, 动态路由也按照最新的加载解析
          next({ ...to, replace: true })
        }
        catch (e) {
          // token过期, token伪造
          store.dispatch('clearLogin')
          next('/sign/login')
        }
      }
    }
  } 
  // 没有 token 的情况
  else {
    // 看是否是不对外开放的后台页面
    if (to.path.indexOf('/my') === 0) {
      next('/sign/login')
    }
    else {
      next()
    }
  }
})