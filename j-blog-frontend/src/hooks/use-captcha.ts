import { captcha } from "@/api/auth";
import { reactive } from "vue";

function useCaptcha () {
  // 状态：验证码数据
  const captchaData = reactive({
    captchaKey: "", // 验证码唯一识别
    captchaImage: "", // 验证码图片
  });

  // 函数：刷新验证码数据
  const refreshCaptcha = async () => {
    // TODO：请求并获取图形验证码数据
    try {
      const { key, svg } = await captcha()
      captchaData.captchaKey = key
      captchaData.captchaImage = svg
    }
    catch (e) {}
  };

  return { captchaData, refreshCaptcha }
}

export default useCaptcha