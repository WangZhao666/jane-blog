// 引包 dayjs => 安装 Element-plus 组件库, 就会默认自动安装 dayjs (不需要额外安装)
import dayjs from 'dayjs'

/**
 * 将后端给出的字符串格式, 进行日期的格式化
 * 格式: 2021-12-12 12:12
 */
export function formatDate(date: string): string {
  return dayjs(date).format("YYYY-MM-DD HH:mm:ss")
}