// 封装一个函数, 函数用于在后台返回回来的头像地址, 前面进行路径拼接
import { baseURL } from "./request";
import defaultAvatar from '@/assets/avatar.png'

// baseURL + /uploads/xxx.jpg, 如果没有地址, 给他默认头像
/**
 * 获取头像的全路径, 如果参数无值, 给默认头像
 * @param path 后端返回的相对路径
 * @returns 
 */
export function getAvatarImage (path: string): string {
  return path ? baseURL + path : defaultAvatar
}